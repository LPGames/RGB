using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PalettePicker : MonoBehaviour
{
    [SerializeField]
    private ColorPalette[] _colorPalettes;
    private TMP_Dropdown _paletteDropdown;
    private int _currentValue;

    // Start is called before the first frame update
    void Start()
    {
        _currentValue = 0;
        PlayerPrefs.SetInt("colorPaletteKey", _currentValue);
        _paletteDropdown = GetComponent<TMP_Dropdown>();
        _paletteDropdown.options.Clear();
        foreach (var palette in _colorPalettes)
        {
            TMP_Dropdown.OptionData option = new TMP_Dropdown.OptionData(palette.name);
            _paletteDropdown.options.Add(option);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (_paletteDropdown.value != _currentValue)
        {
            _currentValue = _paletteDropdown.value;
            PlayerPrefs.SetInt("colorPaletteKey", _currentValue);
            Debug.Log(_currentValue);
            Debug.Log(_colorPalettes[_currentValue].name);
        }
    }
}

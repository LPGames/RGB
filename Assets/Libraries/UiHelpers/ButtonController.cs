using UnityEngine;
using UnityEngine.EventSystems;

namespace UiHelpers
{
    public class ButtonController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {

        public void OnPointerDown(PointerEventData eventData)
        {
            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                Transform child = gameObject.transform.GetChild(i);
                child.Translate(0f, -4f, 0f);
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                Transform child = gameObject.transform.GetChild(i);
                child.Translate(0f, 4f, 0f);
            }
        }
    }
}
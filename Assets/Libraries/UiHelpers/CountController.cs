using TMPro;
using UnityEngine;

namespace UiHelpers
{
    public class CountController : MonoBehaviour
    {

        private Counter _counter;
        private int _currentCount = 0;

        [SerializeField] private TextMeshProUGUI currentCountText;

        void Start()
        {
            _counter = new Counter();
            _currentCount = _counter.Count;

            UpdateUI();
        }

        private void IncrementCounter()
        {
            _counter.Increase();
            _currentCount = _counter.Count;
            UpdateUI();
        }

        private void DecreaseCounter()
        {
            _counter.Decrease();
            _currentCount = _counter.Count;
            UpdateUI();
        }

        private void UpdateUI()
        {
            currentCountText.text = _currentCount.ToString();
        }

        private void OnEnable()
        {
            ButtonsScript.OnWin += IncrementCounter;
        }

        private void OnDisable()
        {
            ButtonsScript.OnWin -= IncrementCounter;
        }
    }
}
using System.Collections;
using UnityEngine;

namespace UiHelpers
{
    public class JuiceMaster
    {
        public static IEnumerator CameraShake(GameObject camera, float duration, float magnitude)
        {
            Vector3 originalPosition = camera.transform.localPosition;
            float elapsed = 0.0f;
            while (elapsed <= duration)
            {
                float x = Random.Range(-1f, 1f) * magnitude;
                float y = Random.Range(-1f, 1f) * magnitude;

                camera.transform.localPosition = new Vector3(x, y, originalPosition.z);

                elapsed += Time.deltaTime;

                yield return null;
            }

            camera.transform.localPosition = originalPosition;
        }
    }
}
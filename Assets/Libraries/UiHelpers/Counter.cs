using System;

namespace UiHelpers
{
    public class Counter
    {
        public Counter(int count = 0)
        {
            Count = count;
        }

        public int Count { get; protected set; }

        public void Increase()
        {
            Count++;
        }

        public void Decrease()
        {
            if (Count <= 0)
            {
                throw new IndexOutOfRangeException("Counter cannot be less than zero.");
            }

            Count--;
        }
    }
}
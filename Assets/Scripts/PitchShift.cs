using UnityEngine;

public class PitchShift : MonoBehaviour
{
    public float pitchMagnitude = 0.04f;
    private AudioSource _audio;

    private void Start()
    {
        _audio = GetComponent<AudioSource>();
    }

    private void ShiftPitchUp()
    {
        _audio.pitch += pitchMagnitude;
    }

    private void OnEnable()
    {
        ButtonsScript.OnColorSwitch += ShiftPitchUp;
    }

    private void OnDisable()
    {
        ButtonsScript.OnColorSwitch -= ShiftPitchUp;
    }
}

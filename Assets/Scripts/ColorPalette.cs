using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "ScriptableObjects/ColorPalette", order = 1)]
public class ColorPalette : ScriptableObject
{
    [Header("Normal Colors")]
    public Color red;
    public Color green;
    public Color blue;
    
    [Header("Protanopia Colors")]
    public Color proRed;
    public Color proGreen;
    public Color proBlue;
   
    [Header("Deuteranopia Colors")]
    public Color deutRed;
    public Color deutGreen;
    public Color deutBlue;
    
    [Header("Tritanopia Colors")]
    public Color triRed;
    public Color triGreen;
    public Color triBlue;
    
    [Header("Deuteranomaly Colors")]
    public Color deutyRed;
    public Color deutyGreen;
    public Color deutyBlue;
    
    [Header("Notes")]
    public string origin;
}
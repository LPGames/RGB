using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    private const string GameSceneName = "Game";
    private const string MenuSceneName = "Menu";
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private GameObject winMenu;

    private AudioSource _backgroundAudio;
    private bool _isShowing;

    public void StartGame()
    {
        SceneManager.LoadSceneAsync(GameSceneName);
        Time.timeScale = 1;
    }

    public void QuitGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(MenuSceneName);
    }

    public void ClosePauseMenu()
    {
        if (pauseMenu)
        {
            pauseMenu.SetActive(false);
            Time.timeScale = 1;
            _isShowing = false;
        }
    }

    public void ShowWinMenu()
    {
        if (winMenu)
        {
            winMenu.SetActive(true);
            Time.timeScale = 0;

            if (_backgroundAudio)
            {
                _backgroundAudio.Stop();
            }
            _isShowing = true;
        }
    }

    public void ShowPauseMenu()
    {
        if (pauseMenu)
        {
            pauseMenu.SetActive(true);
            Time.timeScale = 1;
            _isShowing = true;
        }
    }

    public void CloseWinMenu()
    {
        if (winMenu)
        {
            winMenu.SetActive(false);
            Time.timeScale = 1;
            _isShowing = false;
        }
    }

    private void Start()
    {
        _backgroundAudio = GetComponent<AudioSource>();
        ButtonsScript.OnEndGame += ShowWinMenu;
    }

    private void OnDestroy()
    {
        ButtonsScript.OnEndGame -= ShowWinMenu;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!pauseMenu && !winMenu)
            {
                return;
            }

            if (_isShowing)
            {
                ClosePauseMenu();
                CloseWinMenu();
            }
            else
            {
                pauseMenu.SetActive(true);
                Time.timeScale = 0;
                _isShowing = true;
            }
        }
    }
}
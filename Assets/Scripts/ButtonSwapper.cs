using UnityEngine;
using UnityEngine.UI;

public class ButtonSwapper : MonoBehaviour
{
    [SerializeField]
    private float duration = 10f;

    private Button _buttonOne;
    private Button _buttonTwo;
    private Vector3 _positionOne;
    private Vector3 _positionTwo;

    public bool isSwapping;
    private const float CloseEnough = 10f;
    private float _progress;

    public void SwapButtons(Button buttonOne, Button buttonTwo)
    {
        _progress = 0f;
        _buttonOne = buttonOne;
        _buttonTwo = buttonTwo;
        _positionOne = buttonOne.transform.localPosition;
        _positionTwo = buttonTwo.transform.localPosition;
        isSwapping = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (isSwapping)
        {
            _progress += Time.fixedDeltaTime / duration;

            Vector3 buttonOnePosition = _buttonOne.transform.localPosition;
            Vector3 movementOne = Vector3.Lerp(buttonOnePosition, _positionTwo, _progress);
            _buttonOne.transform.localPosition = movementOne;

            Vector3 buttonTwoPosition = _buttonTwo.transform.localPosition;
            Vector3 movementTwo = Vector3.Lerp(buttonTwoPosition, _positionOne, _progress);
            _buttonTwo.transform.localPosition = movementTwo;

            bool buttonOneCloseEnough = ButtonIsNearDestination(buttonOnePosition, _positionTwo);
            bool buttonTwoCloseEnough = ButtonIsNearDestination(buttonTwoPosition, _positionOne);
            if (buttonOneCloseEnough || buttonTwoCloseEnough)
            {
                _buttonOne.transform.localPosition = _positionTwo;
                _buttonTwo.transform.localPosition = _positionOne;
                isSwapping = false;
                _buttonOne = default;
                _buttonTwo = default;
            }
        }
    }

    bool ButtonIsNearDestination(Vector3 buttonPosition, Vector3 destination)
    {
        if (Mathf.Abs(buttonPosition.x - destination.x) < CloseEnough)
        {
            return true;
        }

        return false;
    }
}

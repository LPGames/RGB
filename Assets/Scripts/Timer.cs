using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Feedbacks;
using TMPro;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public float timeValue = 90;
    private TextMeshProUGUI _timeText;
    private MMFeedbacks _timeFeedbacks;
    private bool _isFinished;

    public static event Action OnTimeOut;

    private void Start()
    {
        _timeText = GetComponent<TextMeshProUGUI>();
        _timeFeedbacks = GetComponent<MMFeedbacks>();
    }

    void Update()
    {
        if (_isFinished)
        {
            return;
        }

        if (timeValue > 0)
        {
            timeValue -= Time.deltaTime;
        }
        else
        {
            timeValue = 0;
            _isFinished = true;
            OnTimeOut?.Invoke();
        }
        DisplayTime(timeValue);
    }

    void DisplayTime(float timeToDisplay)
    {
        if (timeToDisplay > 0)
        {
            timeToDisplay += 1;
        }

        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);
        _timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    private void AddTime()
    {
        _timeFeedbacks.PlayFeedbacks();
        timeValue += 3;
    }

    private void SubtractTime()
    {
        timeValue -= 2;
    }

    private void OnEnable()
    {
        ButtonsScript.OnWin += AddTime;
        ButtonsScript.OnLose += SubtractTime;
    }

    private void OnDisable()
    {
        ButtonsScript.OnWin -= AddTime;
        ButtonsScript.OnLose -= SubtractTime;
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MoreMountains.Feedbacks;
using TMPro;
using UiHelpers;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class ButtonsScript : MonoBehaviour
{
    ButtonSwapper _buttonSwapper;

    [Header("Buttons")]
    [SerializeField]
    private Button _redButton;
    [SerializeField]
    private Button _greenButton;
    [SerializeField]
    private Button _blueButton;

    [Header("Colors")]
    [SerializeField]
    private ColorPalette _palette;
    [SerializeField]
    private ColorPalette[] _colorPalettes;

    [Header("Juice")]
    [SerializeField]
    private GameObject _camera;
    [SerializeField]
    private float _shakeMagnitude = 0;
    [SerializeField]
    private float _shakeDuration = 0;
    [SerializeField]
    private AudioSource _buttonFail;
    [SerializeField]
    private AudioSource _powerUp;

    [Header("Info")]
    [SerializeField]
    private Animator _conditionAnim;
    [SerializeField]
    private TextMeshProUGUI _conditionText;
    [SerializeField]
    private TextMeshProUGUI _conditionMetaText;

    private int _winCount = 0;

    public static event Action OnColorSwitch;
    public static event Action OnWin;
    public static event Action OnLose;
    public static event Action OnEndGame;

    private bool _redPressed;
    private bool _greenPressed;
    private bool _bluePressed;
    private bool _shuffling;

    private Vector3 _position1;
    private Vector3 _position2;
    private Vector3 _position3;
    private List<Vector3> _positions;
    private List<Vector3> _currentOrder;
    private List<Vector3> _nextOrder;

    void Start()
    {
        int paletteKey = PlayerPrefs.GetInt("colorPaletteKey");
        _palette = _colorPalettes[paletteKey];

        _redButton.onClick.AddListener(RedClicked);
        SetButtonColor(_redButton, _palette.red);

        _greenButton.onClick.AddListener(GreenClicked);
        SetButtonColor(_greenButton, _palette.green);

        _blueButton.onClick.AddListener(BlueClicked);
        SetButtonColor(_blueButton, _palette.blue);

        ResetAllButtons();

        _position1 = _redButton.transform.localPosition;
        _position2 = _greenButton.transform.localPosition;
        _position3 = _blueButton.transform.localPosition;
        _positions = new List<Vector3>() {_position1, _position2, _position3};
        _currentOrder = _positions;

        _buttonSwapper = GetComponent<ButtonSwapper>();
    }

    private void Update()
    {
        if (_shuffling)
        {
            if (_buttonSwapper.isSwapping)
            {
                return;
            }

            _currentOrder = new List<Vector3>() {_redButton.transform.localPosition, _greenButton.transform.localPosition, _blueButton.transform.localPosition};
            if (!_currentOrder.SequenceEqual(_nextOrder))
            {
                Button buttonOne = null;
                Button buttonTwo = null;
                if (_currentOrder[0] != _nextOrder[0])
                {
                    buttonOne = _redButton;
                }
                if (_currentOrder[1] != _nextOrder[1])
                {
                    if (buttonOne == null)
                    {
                        buttonOne = _greenButton;
                    }
                    else
                    {
                        buttonTwo = _greenButton;
                    }
                }
                if (_currentOrder[2] != _nextOrder[2] && buttonTwo == null)
                {
                    buttonTwo = _blueButton;
                }

                _buttonSwapper.SwapButtons(buttonOne, buttonTwo);
            }
            else
            {
                _shuffling = false;
                ResetGame();
            }
        }
    }

    private void OnEnable()
    {
        Timer.OnTimeOut += EndGame;
    }

    private void OnDisable()
    {
        Timer.OnTimeOut -= EndGame;
    }

    void RedClicked()
    {
        if (_redPressed) return;

        _redPressed = true;
        HighlightButton(_redButton);
    }

    void GreenClicked()
    {
        if (_greenPressed) return;

        if (!_redPressed)
        {
            Lose();
            return;
        }
        _greenPressed = true;
        HighlightButton(_greenButton);
    }

    void BlueClicked()
    {
        if (_bluePressed) return;

        if (!_greenPressed)
        {
            Lose();
            return;
        }

        HighlightButton(_blueButton);
        Win();

        // Press the button after win so that win can check for button press and ignore.
        _bluePressed = true;
    }

    void HighlightButton(Button button)
    {
        PlaySound(button);
        TextMeshProUGUI text = button.GetComponentInChildren<TextMeshProUGUI>();
        StartCoroutine(FadeInButtonHighlight(text, 0.25f));
    }

    void FadeButtonHighlights()
    {
        TextMeshProUGUI redText = _redButton.GetComponentInChildren<TextMeshProUGUI>();
        StartCoroutine(FadeOutButtonHighlight(redText));

        TextMeshProUGUI greenText = _greenButton.GetComponentInChildren<TextMeshProUGUI>();
        StartCoroutine(FadeOutButtonHighlight(greenText));

        TextMeshProUGUI blueText = _blueButton.GetComponentInChildren<TextMeshProUGUI>();
        StartCoroutine(FadeOutButtonHighlight(blueText));
    }

    IEnumerator FadeOutButtonHighlight(TextMeshProUGUI text, float fadeTime = 0.75f, bool resetOnComplete = false)
    {
        // @fixme Wait for fade in before fading out.
        yield return new WaitForSeconds(.25f);

        Color initialFaceColor = text.fontMaterial.GetColor("_FaceColor");
        Color initialOutlineColor = text.fontMaterial.GetColor("_OutlineColor");

        float waitTime = 0;
        while (waitTime <= 1)
        {
            text.fontMaterial.SetColor("_FaceColor", Color.Lerp(initialFaceColor, Color.clear, waitTime));
            text.fontMaterial.SetColor("_OutlineColor", Color.Lerp(initialOutlineColor, Color.clear, waitTime));
            yield return null;
            waitTime += Time.deltaTime / fadeTime;
        }

        if (resetOnComplete)
        {
            text.enabled = false;
            text.fontMaterial.SetColor("_FaceColor", initialFaceColor);
            text.fontMaterial.SetColor("_OutlineColor", initialOutlineColor);
        }
    }

    IEnumerator FadeInButtonHighlight(TextMeshProUGUI text, float fadeTime = 0.25f)
    {
        float waitTime = 0;
        while (waitTime < 1)
        {
            text.fontMaterial.SetColor("_FaceColor", Color.Lerp(Color.clear, Color.black, waitTime));
            text.fontMaterial.SetColor("_OutlineColor", Color.Lerp(Color.clear, Color.white, waitTime));
            yield return null;
            waitTime += Time.deltaTime / fadeTime;
        }
    }

    void SetButtonColor(Button button, Color color)
    {
        ColorBlock cb = button.colors;
        cb.pressedColor = color;
        cb.highlightedColor = color;
        cb.normalColor = color;
        cb.selectedColor = color;
        button.colors = cb;
    }

    void ResetAllButtons()
    {
        _redPressed = false;
        _greenPressed = false;
        _bluePressed = false;
        ResetButton(_redButton);
        ResetButton(_blueButton);
        ResetButton(_greenButton);
    }

    void ResetButton(Button button)
    {
        TextMeshProUGUI text = button.GetComponentInChildren<TextMeshProUGUI>();
        text.fontMaterial.SetColor("_FaceColor", Color.clear);
        text.fontMaterial.SetColor("_OutlineColor", Color.clear);
    }

    void Win()
    {
        if (_bluePressed)
        {
            return;
        }
        OnWin?.Invoke();
        _winCount++;

        if (_winCount == 5)
        {
            SwitchColors("deuty");
        }
        else if (_winCount == 10)
        {
            SwitchColors("tri");
        }
        else if (_winCount == 15)
        {
            SwitchColors("deut");
        }
        else if (_winCount == 20)
        {
            SwitchColors("prot");
        }

        ShuffleButtons();
    }

    void Lose()
    {
        ScreenShake(_shakeDuration * 2);
        _buttonFail.pitch = 0.8f;
        _buttonFail.Play();
        ResetGame();
        OnLose?.Invoke();
    }

    void EndGame()
    {
        _buttonFail.Stop();
        _buttonFail.pitch = 0.2f;
        _buttonFail.Play();
        ResetAllButtons();
        ScreenShake();
        _redPressed = true;
        _greenPressed = true;
        _bluePressed = true;
        Time.timeScale = 0;
        OnEndGame?.Invoke();
    }

    void ResetGame()
    {
        ResetAllButtons();
    }

    void ShuffleButtons()
    {
        GenerateOrder();

        if (_nextOrder.SequenceEqual(_currentOrder))
        {
            ShuffleButtons();
            return;
        }

        _shuffling = true;

        FadeButtonHighlights();
    }

    Vector3 PickOrder()
    {
        Vector3 order = _positions[0];
        if (_positions.Count > 1)
        {
            int i = Random.Range(0, _positions.Count);
            order = _positions[i];
            _positions.Remove(order);
        }
        return order;
    }

    void GenerateOrder()
    {
        _positions = new List<Vector3>() {_position1, _position2, _position3};
        _nextOrder = new List<Vector3>();
        _nextOrder.Add(PickOrder());
        _nextOrder.Add(PickOrder());
        _nextOrder.Add(PickOrder());
    }

    void SwitchColors(string key = "pro")
    {
        _powerUp.Play();
        ScreenShake();
        OnColorSwitch?.Invoke();
        switch (key)
        {
            case "pro":
                _conditionText.text = "Protanopia";
                SetButtonColor(_redButton, _palette.proRed);
                SetButtonColor(_greenButton, _palette.proGreen);
                SetButtonColor(_blueButton, _palette.proBlue);
                break;

            case "deut":
                _conditionText.text = "Deuteranopia";
                SetButtonColor(_redButton, _palette.deutRed);
                SetButtonColor(_greenButton, _palette.deutGreen);
                SetButtonColor(_blueButton, _palette.deutBlue);
                break;

            case "tri":
                _conditionText.text = "Tritanopia";
                SetButtonColor(_redButton, _palette.triRed);
                SetButtonColor(_greenButton, _palette.triGreen);
                SetButtonColor(_blueButton, _palette.triBlue);
                break;

            case "deuty":
                _conditionText.text = "Deuteranomaly";
                SetButtonColor(_redButton, _palette.deutyRed);
                SetButtonColor(_greenButton, _palette.deutyGreen);
                SetButtonColor(_blueButton, _palette.deutyBlue);
                break;
        }

        MMFeedbacks conditionFeedbacks = _conditionAnim.GetComponent<MMFeedbacks>();
        if (key == "pro")
        {
            _conditionAnim.gameObject.SetActive(true);
            conditionFeedbacks.Initialization();
            conditionFeedbacks.PlayFeedbacks();
        }
        else
        {
            _conditionAnim.SetTrigger("Grow");
            conditionFeedbacks.PlayFeedbacks();
        }
    }

    void ScreenShake(float duration = 0, float magnitude = 0)
    {
        duration = duration == 0 ? _shakeDuration : duration;
        magnitude = magnitude == 0 ? _shakeMagnitude : magnitude;

        StartCoroutine(JuiceMaster.CameraShake(_camera, duration, magnitude));
    }

    void PlaySound(Button button)
    {
        AudioSource audio = button.GetComponent<AudioSource>();
        audio.Play();
    }
}
